//
//  TopListTableViewController.swift
//  Demo_app
//
//  Created by ywang on 2021/02/03.
//

import UIKit

final class TopListTableViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension TopListTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopItem", for: indexPath)
        cell.textLabel?.text = TopListItemType.allCases[indexPath.row].rawValue
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TopListItemType.allCases.count
    }
}

extension TopListTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        tableView.deselectRow(at: indexPath, animated: true)
        // TODO: 後で各画面への遷移を実装
    }
}
