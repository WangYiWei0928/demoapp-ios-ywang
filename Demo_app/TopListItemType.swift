//
//  TopListItem.swift
//  Demo_app
//
//  Created by ywang on 2021/02/04.
//

import Foundation

enum TopListItemType: String, CaseIterable {
    case picker = "Picker"
    case map = "Map"
    case video = "Video"
    case webView = "Web View"
    case viewPager = "View Pager"
    case form = "Form"

    var viewControllerName: String {
        switch self {
        case .picker: return "PickerViewController"
        case .map: return "MapViewController"
        case .video: return "VideoController"
        case .webView: return "WebViewController"
        case .viewPager: return "ViewPagerController"
        case .form: return "FormViewController"
        }
    }
}
